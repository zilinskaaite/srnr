install.packainstall.packages(pacman)
library(pacman)
p_load(Biostrings, data.table, dplyr, TmCalculator, stringi, ggplot2)

d <- fread("INPUTS/APERO/PnG2.txt") %>% 
  .[, c("Position", "lg", "str"), with=FALSE] %>%
  .[, Position := round(Position)] %>%
  .[, end := Position + round(lg)] %>%
  .[, ID := paste0(Position, "_", end, "_", str)] %>%
  .[, chr := "NC_009004.1"] %>%
  .[, qual := 50] %>%
  .[, c("chr", "Position", "end", "qual", "str", "ID"), with=FALSE]

write.csv(d, "OUTPUT/BED/PnG2.csv", row.names = FALSE, quote = FALSE)
cmd <- paste0("cat OUTPUT/BED/PnG2.csv | tr ',' '\t' > OUTPUT/BED/PnG2.bed")
cmd <- paste0("sed -i 1d OUTPUT/BED/PnG2.bed")
cmd <- paste0("bedtools getfasta -fi REF/NC_009004.fasta -bed  OUTPUT/BED/PnG2.bed -fo OUTPUT/FASTA/PnG2.fasta")

###############find terminators######
./findterm "PnG1.fasta" "-11" "0" "findterm.cfg" "-over" -o PnG1_term.rds
###############find promotors######
./bprom PnG1.fasta PnG1_prom.rds

####################Feature_table#########
seq <- Biostrings::readDNAStringSet("OUTPUT/FASTA/PnG2.fasta")

feature_table <- data.table(name=names(seq),
                            
                            seq_name=as.character(seq, use.names=TRUE),
                            
                            length=width(seq),
                            
                            alphabetFrequency(seq, baseOnly=TRUE)) %>%
  
  .[, GC := sapply(seq_name, GC)] %>%
  
  .[, TM_GC := sapply(seq_name, Tm_GC)] %>%
  
  .[, first15 := substr(seq_name, 1, 15)] %>%
  
  .[, last15 := stri_reverse(seq_name) %>% substr(., 1, 15) %>% stri_reverse(.)]

write.csv(feature_table, "OUTPUT/FEATURES/PnG2_features.csv", row.names = FALSE, quote = FALSE)

######Feature table graphs###

feature_table %>%
  
  .[, c("name", "length", "GC", "TM_GC"), with=FALSE] %>%
  
  melt(., id.vars="name") %>%
  
  setnames(., c("name", "feature", "value")) %>%
  
  ggplot(., aes(x=feature, y=value)) +
  
  geom_violin() +
  
  ggtitle("sRNAs features") +
  
  theme_Publication()



#########################Theme settings###############
theme_Publication <- function(base_size=14, base_family="helvetica") {
  library(grid)
  library(ggthemes)
  (theme_foundation(base_size=base_size, base_family=base_family)
    + theme(plot.title = element_text(face = "bold",
                                      size = rel(1.2), hjust = 0.5),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold",size = rel(1)),
            axis.title.y = element_text(angle=90,vjust =2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(), 
            axis.line = element_line(colour="black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour="#f0f0f0"),
            panel.grid.minor = element_blank(),
            legend.key = element_rect(colour = NA),
            legend.position = "none",
            legend.direction = "horizontal",
            legend.key.size= unit(0.2, "cm"),
            legend.margin = unit(0, "cm"),
            legend.title = element_text(face="italic"),
            plot.margin=unit(c(10,5,5,5),"mm"),
            strip.background=element_rect(colour="#f0f0f0",fill="#f0f0f0"),
            strip.text = element_text(face="bold")
    ))
  
}

scale_fill_Publication <- function(...){
  library(scales)
  discrete_scale("fill","Publication",manual_pal(values = c("#386cb0","#fdb462","#7fc97f","#ef3b2c","#662506","#a6cee3","#fb9a99","#984ea3","#ffff33")), ...)
  
}

scale_colour_Publication <- function(...){
  library(scales)
  discrete_scale("colour","Publication",manual_pal(values = c("#386cb0","#fdb462","#7fc97f","#ef3b2c","#662506","#a6cee3","#fb9a99","#984ea3","#ffff33")), ...)
  
}

###ALL feature tables#######

library(pacman)
library(cowplot)
p_load(Biostrings, data.table, dplyr, TmCalculator, stringi, ggplot2, gridExtra)

Lys1_Features <- fread("OUTPUT/FEATURES/Lys1_features.csv")
Lys1_Features$Sample <- 'Lys1'
Lys2_Features <- fread("OUTPUT/FEATURES/Lys2_features.csv")
Lys2_Features$Sample <- 'Lys2'
PnG1_Features <- fread("OUTPUT/FEATURES/PnG1_features.csv")
PnG1_Features$Sample <- 'PnG1'
PnG2_Features <- fread("OUTPUT/FEATURES/PnG2_features.csv")
PnG2_Features$Sample <- 'PnG2'
Nk_Features <- fread("OUTPUT/FEATURES/Nk_features.csv")
Nk_Features$Sample <- 'Nk'
sRNR_Features <- fread("OUTPUT/FEATURES/sRNR_features.csv") %>% .[, Sample := "sRNR"]


table <- rbind(fill=TRUE, Lys1_Features, 
               Lys2_Features, 
               Nk_Features, 
               PnG1_Features, 
               PnG2_Features, 
               sRNR_Features)

pGC <- table %>%
  .[, c( "name", "GC", "Sample"), with=FALSE] %>%
  ggplot(., aes(x=Sample, y=GC, fill=Sample)) +
    geom_violin() +
    labs(tag="A") +
    geom_boxplot(width=0.1, color="grey", alpha=0.5) +
    theme_Publication()

pTm_GC <- table %>%
  .[, c( "name", "TM_GC", "Sample"), with=FALSE] %>%
  ggplot(., aes(x=Sample, y=TM_GC, fill=Sample)) +
    geom_violin() +
    labs(tag="B") +
    geom_boxplot(width=0.1, color="grey", alpha=0.5) +
    theme_Publication()

pLength <- table %>%
  .[, c( "name", "length", "Sample"), with=FALSE] %>%
  ggplot(., aes(x=Sample, y=length, fill=Sample)) +
    geom_violin() +
    labs(tag="C") +
    geom_boxplot(width=0.1, color="grey", alpha=0.5) +
    theme_Publication()

png(file="FeaturesLong.png")
grid.arrange(pGC, pTm_GC, pLength, widths = c(5,5),ncol=2)
dev.off()

table <- rbind(fill=TRUE, Lys1_Features, 
               Lys2_Features, 
               Nk_Features, PnG1_Features, PnG2_Features, sRNR_Features) %>% .[length <= 500, ]

pGC <- table %>%
  .[, c( "name", "GC", "Sample"), with=FALSE] %>%
  ggplot(., aes(x=Sample, y=GC, fill=Sample)) +
    geom_violin() +
    labs(tag="A") +
    geom_boxplot(width=0.1, color="grey", alpha=0.5) +
    theme_Publication()

pTm_GC <- table %>%
  .[, c( "name", "TM_GC", "Sample"), with=FALSE] %>%
  ggplot(., aes(x=Sample, y=TM_GC, fill=Sample)) +
    geom_violin() +
    labs(tag="B") +
    geom_boxplot(width=0.1, color="grey", alpha=0.5) +
    theme_Publication()

pLength <- table %>%
  .[, c( "name", "length", "Sample"), with=FALSE] %>%
  ggplot(., aes(x=Sample, y=length, fill=Sample)) +
    geom_violin() +
    labs(tag="C") +
    geom_boxplot(width=0.1, color="grey", alpha=0.5) +
    theme_Publication()

png(file="Features500.png")
grid.arrange(pGC, pTm_GC, pLength, widths = c(5,5),ncol=2)
dev.off()
